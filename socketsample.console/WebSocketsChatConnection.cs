﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Sockets.Client;
using Microsoft.Extensions.Logging;

namespace socketsample.console
{
    internal class WebSocketsChatConnection
    {
        private static HttpConnection _connection;
        private static CancellationTokenSource _cts;
        private static bool _connectionIsClosed;
        internal static void Register()
        {
            Task.Factory.StartNew(async () => { await ExecuteAsync(""); });            
        }

        public static async Task<int> ExecuteAsync(string baseUrl)
        {
            baseUrl = string.IsNullOrEmpty(baseUrl) ? "http://localhost:53902/chat" : baseUrl;

            var loggerFactory = new LoggerFactory();
            var logger = loggerFactory.CreateLogger<Program>();      

            Console.WriteLine($"Connecting to {baseUrl}...");
            _connection = new HttpConnection(new Uri(baseUrl), loggerFactory);
            try
            {
                _connection.Received += ConnectionOnReceivedAsync;
                _connection.Closed += ConnectionOnClosed;
                await _connection.StartAsync();
                _connectionIsClosed = false;
                Console.WriteLine($"Connected to {baseUrl}");
                _cts = new CancellationTokenSource();
                Console.CancelKeyPress += async (sender, a) =>
                {
                    a.Cancel = true;
                    await _connection.DisposeAsync();
                };

                var userName = new WebSocketUserName(){
                    MessageType = "USERNAME",
                    UserName = "Console"
                };

                await _connection.SendAsync(userName.Serialize(), _cts.Token);

                var wsSystem = new WebSocketModel
                {
                    MessageType = "SYSTEMNOTIFICATION",
                    UserName = "Console",
                    Message = $"Console has joined this session!",
                    TimeStamp = DateTime.Now
                };

                await _connection.SendAsync(wsSystem.Serialize(), _cts.Token);

                while (!_connectionIsClosed)
                {
                    var line = await Task.Run(() => Console.ReadLine(), _cts.Token);

                    if (line == null)
                    {
                        line = "empty message";
                    }

                    var message = new WebSocketModel()
                    {
                        Message = line,
                        MessageType = "COMMUNICATION"
                    };

                   
                    await _connection.SendAsync(message.Serialize(), _cts.Token);
                }
            }
            catch (AggregateException aex) when (aex.InnerExceptions.All(e => e is OperationCanceledException))
            {
            }
            catch (OperationCanceledException)
            {
            }
            finally
            {
                await _connection.DisposeAsync();
            }
            return 0;
        }

        private static async Task ConnectionOnClosed(Exception exception)
        {
           _connectionIsClosed = false;
        }

        private static async Task ConnectionOnReceivedAsync(byte[] bytes)
        {
            var text = Encoding.UTF8.GetString(bytes);

            if (WebSocketModel.TryFromJsonString(text, out WebSocketModel wsModel))
            {
                switch (wsModel.MessageType)
                {
                    case "COMMUNICATION":
                    case "HOTKEYURL":
                        if (wsModel.UserName != "Console")
                        {
                            await Console.Out.WriteLineAsync($"[{wsModel.TimeStampAsString}] {wsModel.UserName}: {wsModel.Message}");
                        }
                        break;
                    case "SYSTEMNOTIFICATION":
                        await Console.Out.WriteLineAsync($"-------- [{wsModel.TimeStampAsString}] {wsModel.Message} --------");
                        break;
                }         
            }       
        }
    }
}
