﻿using System;
using System.Text;
using Newtonsoft.Json;

namespace socketsample.console
{
    public class WebSocketModel
    {
        [JsonProperty("messageType")]
        public string MessageType { get; set; }
        [JsonProperty("connectionId")]
        public Guid ConnectionId { get; set; }
        [JsonProperty("userName")]
        public string UserName { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("timeStamp")]
        public DateTime TimeStamp { get; set; }

        [JsonProperty("timeStampAsString")]
        public string TimeStampAsString => TimeStamp.ToString("u");

        public string ToJsonString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static WebSocketModel FromJsonString(string jsonString)
        {
            return JsonConvert.DeserializeObject<WebSocketModel>(jsonString);
        }

        public byte[] Serialize()
        {
            return Encoding.UTF8.GetBytes(this.ToJsonString());
        }

        public static bool TryFromJsonString(string jsonString, out WebSocketModel obj)
        {
            try
            {
                obj = FromJsonString(jsonString);
                return true;
            }
            catch (Exception e)
            {
                obj = null;
                return false;
            }
        }

        public static bool TryToJsonString(WebSocketModel obj, out string value)
        {
            try
            {
                value = obj.ToJsonString();
                return true;
            }
            catch (Exception e)
            {
                value = null;
                return false;
            }
        }

        public static bool TrySerialize(WebSocketModel obj, out byte[] bytes)
        {
            try
            {
                bytes = obj.Serialize();
                return true;
            }
            catch (Exception e)
            {
                bytes = null;
                return false;
            }
        }
    }

    public class WebSocketUserName
    {
        [JsonProperty("messageType")]
        public string MessageType { get; set; }
        [JsonProperty("userName")]
        public string UserName { get; set; }

        public string ToJsonString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static WebSocketUserName FromJsonString(string jsonString)
        {
            return JsonConvert.DeserializeObject<WebSocketUserName>(jsonString);
        }

        public byte[] Serialize()
        {
            return Encoding.UTF8.GetBytes(this.ToJsonString());
        }

        public static bool TryFromJsonString(string jsonString, out WebSocketUserName obj)
        {
            try
            {
                obj = FromJsonString(jsonString);
                return true;
            }
            catch (Exception e)
            {
                obj = null;
                return false;
            }
        }

        public static bool TryToJsonString(WebSocketUserName obj, out string value)
        {
            try
            {
                value = obj.ToJsonString();
                return true;
            }
            catch (Exception e)
            {
                value = null;
                return false;
            }
        }

        public static bool TrySerialize(WebSocketUserName obj, out byte[] bytes)
        {
            try
            {
                bytes = obj.Serialize();
                return true;
            }
            catch (Exception e)
            {
                bytes = null;
                return false;
            }
        }
    }
}
