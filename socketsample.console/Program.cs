﻿using System;

namespace socketsample.console
{
    public class Program
    {
        public static void Main(string[] args)
        {
           WebSocketsChatConnection.Register();

            while (true)
            {
                if (!Console.KeyAvailable)
                {
                    if (Console.ReadKey(true).Key == ConsoleKey.Escape)
                    {
                        Environment.Exit(0);
                    }
                }
            }
        }
    }
}