# README #

This is a sample project to show how you can connect different users over websockets, handle different types of objects and
adding multiple console applications (netcore/framework).

### Needs ###

 - aspcore 2.0
 - visualstudio 2017

### How do I get set up? ###

 1. Load the project in visualstudio 2017
 2. Manage nugetpackages for the project, by rightclicking the solution in the solutionexplorer
 3. Set Startup projects, by rightclicking the solution in the solutionexplorer, select multiple projects ( order => 1 socketsample 2 socketsample.console 3 socketsample.hotkeyapp )
 4. Press start
 
 To use the hotkey app, copy an url and press alt + a.


Questions? 

Contact [Mante Bridts](mailto:mante.bridts@district01.be).