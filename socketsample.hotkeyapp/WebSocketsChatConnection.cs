﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using socketsample.console;

namespace socketsample.hotkeyapp
{
    internal class WebSocketsChatConnection
    {
        private static ClientWebSocket _connection;
        private static CancellationTokenSource _cts;
        internal static void Register()
        {
            Task.Factory.StartNew(async () => { await ExecuteAsync(""); });
        }

        internal static void SendUrl(string url)
        {
            var wsSystem = new WebSocketModel
            {
                MessageType = "HOTKEYURL",
                Message = $"{url}",
            };

            ArraySegment<byte> bytesToSend = new ArraySegment<byte>(wsSystem.Serialize());
            _connection.SendAsync(bytesToSend, WebSocketMessageType.Text, true, CancellationToken.None);
        }

        public static async Task<int> ExecuteAsync(string baseUrl)
        {
            using (var ws = new ClientWebSocket())
            {
                baseUrl = string.IsNullOrEmpty(baseUrl) ? "ws://localhost:53902/chat" : baseUrl;

                Console.WriteLine($"Connecting to {baseUrl}...");
                _cts = new CancellationTokenSource();

                try
                {
                    await ws.ConnectAsync(new Uri(baseUrl), CancellationToken.None);
                    _connection = ws;
                    Console.WriteLine($"Connected to {baseUrl}");
                    Console.CancelKeyPress += async (sender, a) =>
                    {
                        a.Cancel = true;
                        ws.Dispose();
                    };

                    var userName = new WebSocketUserName()
                    {
                        MessageType = "USERNAME",
                        UserName = "HotkeyApp"
                    };

                    ArraySegment<byte> bytesToSend = new ArraySegment<byte>(userName.Serialize());
                    await ws.SendAsync(bytesToSend, WebSocketMessageType.Text, true, CancellationToken.None);

                    var wsSystem = new WebSocketModel
                    {
                        MessageType = "SYSTEMNOTIFICATION",
                        UserName = "Console",
                        Message = $"HotkeyApp has joined this session!",
                        TimeStamp = DateTime.Now
                    };

                    ArraySegment<byte> bytesToSend2 = new ArraySegment<byte>(wsSystem.Serialize());
                    await ws.SendAsync(bytesToSend2, WebSocketMessageType.Text, true, CancellationToken.None);

                    while (true)
                    {
                        var line = await Task.Run(() => Console.ReadLine(), CancellationToken.None);
                    }
                }
                catch (AggregateException aex) when (aex.InnerExceptions.All(e => e is OperationCanceledException))
                {
                }
                catch (OperationCanceledException)
                {
                }
                finally
                {
                    ws.Dispose();
                }
                return 0;
            }
            
        }
    }
}
