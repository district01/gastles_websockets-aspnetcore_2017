﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace socketsample.hotkeyapp
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            HotKeyManager.RegisterHotKey(Keys.A, KeyModifiers.Alt);
            HotKeyManager.HotKeyPressed += new EventHandler<HotKeyEventArgs>(HotKeyManager_HotKeyPressed);
            WebSocketsChatConnection.Register();
            Console.ReadLine();
        }
        
        static void HotKeyManager_HotKeyPressed(object sender, HotKeyEventArgs e)
        {           
            var text = Clipboard.GetText();
            Console.WriteLine(text);
            WebSocketsChatConnection.SendUrl(text);        
        }
    }
}
