﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Sockets;
using socketsample.Models;

namespace socketsample.Endpoints
{
    public class MessagesEndPoint : EndPoint
    {
        public ConnectionList Connections { get; } = new ConnectionList();

        public override async Task OnConnectedAsync(ConnectionContext connection)
        {
            Connections.Add(connection);

            await Broadcast($"{connection.ConnectionId} connected ({connection.Metadata[ConnectionMetadataNames.Transport]})");

            try
            {
                while (await connection.Transport.In.WaitToReadAsync())
                {
                    if (connection.Transport.In.TryRead(out var buffer))
                    {
                        var text = Encoding.UTF8.GetString(buffer);

                        if (WebSocketUserName.TryFromJsonString(text, out WebSocketUserName wsUserName) && wsUserName.MessageType == "USERNAME")
                        {
                            var currentConnection = Connections.First(c => c.ConnectionId == connection.ConnectionId);
                            currentConnection.Metadata.Add("WsUserName", wsUserName);
   
                            var wsCanSee = new WebSocketUserCanSee()
                            {
                                MessageType = "USERNAMEACCEPTED",
                                HasUserName = true,
                                UserName = wsUserName.UserName
                            };
                            await currentConnection.Transport.Out.WriteAsync(wsCanSee.Serialize());

                            var wsSystem = new WebSocketModel
                            {
                                MessageType = "SYSTEMNOTIFICATION",
                                UserName = wsUserName.UserName,
                                Message = $"{wsUserName.UserName} has joined this session!",
                                TimeStamp = DateTime.Now
                            };

                            await Broadcast(wsSystem.Serialize());
                        }
                        if (WebSocketModel.TryFromJsonString(text, out WebSocketModel wsModel) && wsModel.MessageType == "COMMUNICATION" || wsModel.MessageType == "HOTKEYURL")
                        {
                            var currentConnection = Connections.First(c => c.ConnectionId == connection.ConnectionId);

                            wsModel.UserName = ((WebSocketUserName)currentConnection.Metadata["WsUserName"]).UserName;
                            wsModel.ConnectionId = new Guid(currentConnection.ConnectionId);
                            wsModel.TimeStamp = DateTime.Now;

                            await Broadcast(wsModel.Serialize());
                        }
                    }
                }
            }
            finally
            {
                var currentConnection = Connections.First(c => c.ConnectionId == connection.ConnectionId);
                var currentUserName = ((WebSocketUserName)currentConnection.Metadata["WsUserName"]).UserName;

                var wsSystem = new WebSocketModel
                {
                    MessageType = "SYSTEMNOTIFICATION",
                    UserName = ((WebSocketUserName)currentConnection.Metadata["WsUserName"]).UserName,
                    Message = $"{currentUserName} has left this session!",
                    TimeStamp = DateTime.Now
                };

                await Broadcast(wsSystem.Serialize());
                Connections.Remove(connection);
            }
        }

        private Task Broadcast(string text)
        {
            return Broadcast(Encoding.UTF8.GetBytes(text));
        }

        private Task Broadcast(byte[] payload)
        {
            var tasks = new List<Task>(Connections.Count);

            foreach (var c in Connections)
            {
                tasks.Add(c.Transport.Out.WriteAsync(payload));
            }

            return Task.WhenAll(tasks);
        }
    }
}